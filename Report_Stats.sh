#!/bin/bash
#
#Report_Stats - Generate Rpt from Captured Perf Stats
#

#Set Script Variables
REPORT_FILE=$HOME/github/script/capstats.csv
TEMP_FILE=$HOME/capstats.html

DATE=`date +%y/%m/%d`

MAIL=`which mutt`
MAIL_TO="joy_lmx@163.com"

#######################
#Create Report Header
echo "<html><body><h2>Report for $DATE</h2>" > $TEMP_FILE
echo "<table border=\"1\">" >> $TEMP_FILE
echo "<tr><td>Date</td><td>Time</td><td>Users</td>" >> $TEMP_FILE
echo "<td>Load</td><td>Free Memory</td><td>%CPU Idle</td></tr>" >> $TEMP_FILE


#######################
#Place Performance Stats in Report

cat $REPORT_FILE | awk -F, '{
printf "<tr><td>%s</td><td>%s</td><td>%s</td>", $1, $2, $3;
printf "<td>%s</td><td>%s</td><td>%s</td></tr>\n", $4, $5, $6;
}' >> $TEMP_FILE
echo "</table></body></html>" >> $TEMP_FILE

#######################
# Mail Performance Report & Clean up
$MAIL -a $TEMP_FILE -s "Performance Report $DATE" -- $MAIL_TO < /dev/null

#
rm -rf $TEMP_FILE
rm -rf $REPORT_FILE
