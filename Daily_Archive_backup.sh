#!/bin/bash
#
#Daily Archive - Archive designated files & directories
#

#gather current date
DATE=$(date '+%y%m%d')

#set archive file name
FILE=archive$DATE.tar.gz

#set configuration and destination file

CONFIG_FILE=/home/lalor/backup/Files_To_Backup
DESTINATION=/home/lalor/backup/$FILE

#########Main Script############

#Check Backup Config file exists

#make sure the config file still exists
if [[ -f $CONFIG_FILE ]]; then 
	echo  #if it exists. do nothing but continue on
else
	echo
	echo "$CONFIG_FILE does not exist"
	echo "Backup not completed due to missing Configuration file"
	echo
	exit
fi


#Build the names of all the files to backup
FILE_NO=1  #start on line 1 of config file
exec < $CONFIG_FILE #redirect std input to name of config file

read FILE_NAME #read 1st record
while [[ $? -eq 0 ]]; do  #create list of files to backup
	#make sure the file or directory exists
	if [ -f $FILE_NAME -o -d $FILE_NAME ]; then
		#if file exists, add its name to the list
		FILE_LIST="$FILE_LIST $FILE_NAME"
	else
		#if file doesn't exist, issue warning
		echo
		echo "$FILE_NAME, does not exists."
		echo "Obviously, I will not include it in this archive"
		echo "It is listed on line $FILE_NO of the config file"
		echo "Continuing to build archive list..."
		echo
	fi
	FILE_NO=$[ $FILE_NO + 1] #Increase Line / File number by one
	read FILE_NAME  #Read next record
done

#backup the files and Compress Archive
tar -czf $DESTINATION $FILE_LIST 2>/dev/null
