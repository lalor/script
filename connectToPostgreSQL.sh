#!/bin/bash
#test connecting to the psql server
#-H 为可选，以html 格式输出
#\l 列出所有的数据库
#\dt 列出当前数据库中所有的表

CREATEDB="/usr/local/pgsql/bin/createdb"
#CREATEDB=`which createdb`

PSQL="/usr/local/pgsql/bin/psql"
#PSQL=`which psql`

#create database

DATABASENAME=test2
$CREATEDB $DATABASENAME

echo "create table"
$PSQL $DATABASENAME << EOF
create table employees
(
empid int not null,
lastname varchar(30),
firstname varchar(30),
salary float,
primary key(empid)
);
EOF

echo "show databases && tables"
$PSQL $DATABASENAME << EOF
\l
\dt
EOF

echo "insert data"
$PSQL $DATABASENAME << EOF
insert into employees values(1, 'Blum', 'Rich', 2500.00);
insert into employees values(2, 'ming', 'lai', 4500.00);
insert into employees values(3, 'hw', 'liao', 5500.00);
EOF

echo "display data with html format"
$PSQL $DATABASENAME -H << EOF
select * from employees where salary > 4000;
EOF
