#!/bin/bash
# Mingxing LAI
# me@mingxinglai.com

function main()
{

  user="rds-user"
  port="1046"
  ppk="/root/lmx/rds.private"

  if [ $# -ne 2 ]; then
      echo "Usage: $0 iplist rdsAgent"
      exit 1
  fi

  IPLIST=$1
  Agent=$2
  agent_safe="$Agent/agent_safe"
  if [ ! -x $agent_safe ]; then
      echo "please ensure $agent_safe exists with execute permission"
      exit 1
  fi

  for ip in `cat $IPLIST`
  do
      echo "scp -P $port -i $ppk -r $Agent  $user@$ip:~/rdsAgent"
      scp -P $port -i $ppk -r $Agent  $user@$ip:~/AgentNew
      ssh -t -p $port -i $ppk $user@$ip << EOF
      [ -e AgentBackup ] && rm -rf AgentBackup
      mv rdsAgent AgentBackup
      mv AgentNew rdsAgent
      cp ~/AgentBackup/config/RDSAgent.cnf ~/rdsAgent/config/RDSAgent.cnf
      ps aux | grep main | grep -v grep | awk '{print \$2}' | xargs sudo kill -9
      $agent_safe
EOF
  done

}

main $*
