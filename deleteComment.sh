#!/bin/sh -f
#
#delete comment from source file(c/c++)
#


function del_comment_file()
{
        #C++模式注释的删除。
        #Delete the line begins with //
        #首先删除//在行首的行        
        sed -i '/^[ \t]*\/\//d' $file

        #Delete the line ends with //
        #注意已经除去了 //在行首的情况,下面匹配除去URL的//部分,因为代码中有一部分中有
        #URL，形如fun（"ftp://"）
        sed -i 's/\/\/[^"]*//' $file

        #删除以C语言中注释只在一行的行
        sed -i 's/\/\*.*\*\///' $file

        #Delete the lines between /* and */
        #删除C语言中注释跨行的情况
        sed -i '/^[ \t]*\/\*/,/.*\*\//d' $file
}


function del_comment()
