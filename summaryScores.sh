#!/bin/bash
#filename: summary.sh
#utility:For the summary of students' test scores
#useage: ./summary.sh scores/19920112203610*
#date: 2012年 06月 12日 星期二 14:03:58 CST
#
#description:在scores目录下存在很多子目录，子目录是以学生的学号命名的
#以学号命名的目录下放的是学生提交的程序，分别命名为
#error1,cpp, error2.cpp, prog1.cpp, prog2.cpp, prog3.cpp, prog4.cpp
#准备工作是先人工修改这些程序，并且程序文件中以注释的方式给出学生的成绩
#形如：// score 6
#打分的语句(// score 6)可以出现的文件中的任何位置，但是只能出现一次
#每个文件都需要出现一次，以确保改卷没有遗漏（脚本会检查是否对每个文件都打分了)
#最后生成一个如下的文本文件
#
#学号			error1	error2	prog1	prog2	prog3	prog4
#19920112203546		6		4		6		6		6		0	
#19920112203547		4		6		6		6		5		0	
#19920112203548		4		4		6		0		5		4	
#19920112203549		5		6		6		6		6		6	


#先检查是否已经对文件打分了
files[1]=error1.cpp
files[2]=error2.cpp
files[3]=prog1.cpp
files[4]=prog2.cpp
files[5]=prog3.cpp
files[6]=prog4.cpp

i=0
for dir in $@
do
	for res in ${files[*]}
	do
		path=$dir/$res
		temp=`egrep 'score' $path 2> /dev/null`
		if [[ -z $temp  ]]; then
			echo $path
			let i++
		fi
	done
done

if [[ $i -eq 0 ]]; then
	echo "所有文件都已经打分了"
else
	echo " $i files 没有打分或没有批改"
	exit #退出
fi


echo "rm -rf res.txt"
rm -rf res.txt

result=""
res=""

echo "开始统计成绩"


#如果程序运行到了这里，则说明所有试题已经打分，下面开始统计成绩
for dir in $@
do
	sno=`echo $dir | sed 's/^[^0-9]*//g'`
	result=$sno"\t"
	for file in ${files[*]}
	do
		path=$dir/$file
		score=`egrep 'score' $path 2> /dev/null | sed 's/^[^/]*//g' | sed 's/^[^0-9]*//g'`
		res=${result}"\t"${score}"\t"
		result=$res
	done

	echo -e "$result" >> result$$.txt
done

#不知道为什么，用echo 输出数据到文件，文件里很多，下面这条语句就用来处理这个问题
sed -i 's///g' result$$.txt

#第一行
echo -e "学号\t\t\terror1\terror2\tprog1\tprog2\tprog3\tprog4" >> res.txt
sort -nk 1 result$$.txt >> res.txt #按学号排序
rm -rf result$$.txt

echo "成功，请查看当前目录下的res.txt 文件"

###########################此后无内容###########################
