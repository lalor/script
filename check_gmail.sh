#check new mail in terminal

#download xml file 
curl -u USERNAME:PASSWORD --silent https://mail.google.com/mail/feed/atom |

#parse email entry
awk 'BEGIN  { flag=0 } 
/<entry>/   { flag=1; }
flag==1     { print }
/<\/entry>/ { flag=0;print "" }' | 

#print related item
awk 'BEGIN{RS=""; FS="\n"} 
{
	print  "邮件      ：" NR; 
	print "主题      ：" $2; 
	print "发件人    ："$9; 
	print "发件人邮箱："$10; 
	print ""
}' | sed 's/<\/.*>$//g' | sed 's/^<.*>//g' #remove tags

