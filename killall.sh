#!/bin/bash

if [ $# -ne 1 ]; then
    echo "Usage: $0 pid"
   exit
fi


if kill -s 0 $1
then
    echo -e "\e[0;34;1m\nReady to kill $1 and it's subprocess\e[0m"
else
    echo "$1 is not a valid pid, Is it has been killed ?"
    exit
fi


count1=`ps -ef | awk '{ print $2, $3 }' | grep -w $1 | wc -l`
subprocess_list1=`ps -ef | awk '{ print $2, $3 }' | grep -w $1 | awk '{ print $1 }'| paste -sd'|'`
count2=`ps -ef | awk '{ print $2, $3 }' | grep -Ew $subprocess_list1 | wc -l`
subprocess_list2=`ps -ef | awk '{ print $2, $3 }' | grep -Ew $subprocess_list1 | awk '{ print $1 }'| paste -sd'|'`


while [ $count1 -lt $count2 ]
do
    count1=$count2
    subprocess_list1=$subprocess_list2
    count2=`ps -ef | awk '{ print $2, $3 }' | grep -Ew $subprocess_list1 | wc -l`
    subprocess_list2=`ps -ef | awk '{ print $2, $3 }' | grep -Ew $subprocess_list1 | awk '{ print $1 }'| paste -sd'|'`
done


ps -ef | grep -Ew "$subprocess_list2" > .process_list
echo 'Preparing to kill these processes:'


while read line;
do
    if echo "$line" | awk '{ print $2, $3 }' | grep -qEw "$subprocess_list2"
    then
        echo $line
    fi
done < .process_list
rm .process_list


echo -e "\e[0;34;1m\nDo you want to kill these processes ?[y/n]\e[0m"
read -t 60 ANSWER
case $ANSWER in
    y|Y|yes|Yes|YES|yEs|yeS|YEs|yES)
        echo -e "\e[0;34;1m\nTry to kill theses processes ( $subprocess_list2 )\e[0m"
        echo "$subprocess_list2" | tr '|' '\n' | xargs kill -9
        sleep 1
        ;;
    *)
        echo "Do not delete any process."
    ;;
esac


count=0
for item in `echo $subprocess_list2 | tr '|' ' '`
do
    kill -s 0 $item &>/dev/null && echo -e "\e[0;31;1mprocess( $item ) is still alive\e[0m" && let count++
done


if [ $count -eq 0  ]; then
    echo -e "\e[0;32;1mSuccessful\e[0m"
fi
