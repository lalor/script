#!/bin/bash

ip_array=("10.180.145.17" "10.180.145.12" "10.180.145.74")
#ip_array=("10.180.145.18")
user="rds-user"
port="22"
ppk="/home/lalor/.ssh/rds-agent-pri-yanlian"


for ip in ${ip_array[*]}
do
    ssh -t -p $port -i $ppk $user@$ip << EOF
sudo sed -i '/^PasswordAuthentication/d' /etc/ssh/sshd_config
sudo sed -i '/^RSAAuthentication/d' /etc/ssh/sshd_config
sudo sed -i '/^PubkeyAuthentication/d' /etc/ssh/sshd_config

sudo echo 'PasswordAuthentication no'| sudo tee -a /etc/ssh/sshd_config >/dev/null
sudo echo 'RSAAuthentication yes'    | sudo tee -a /etc/ssh/sshd_config >/dev/null
sudo echo 'PubkeyAuthentication yes' | sudo tee -a /etc/ssh/sshd_config >/dev/null
echo 'ssh-rsa AAAAB32EAAAADAQABAAABAQDLGZ Generated by Nova%' | sudo tee  ~rds-user/.ssh/authorized_keys
sudo /etc/init.d/ssh restart
EOF
done
