
#
#
#download books from books.qq.com
#
#
if [[ $# -ne 2 ]]; then
    echo "Usage: $0 URL FILENAME"
    exit 1
fi

output=`curl -I $1 -s| grep "HTTP/.*OK"`;

if [[ -z $output ]]; then
    echo "Please input a correct url"
    exit 1
fi

url=$1
path=`echo $url | sed "s/[^/]*$//g"`

i="1"
echo "first arg:$1"
echo "path:$path"

url="$path$i.shtml"
echo "url:$url"
output=`curl -I $url -s | grep "HTTP/.*OK"`;
echo $output

while [[ ! -z $output ]]; do
    
    echo "begin to download:$url"
    curl "$url" -s > $i.shtml 
    cat ./$i.shtml | grep "<br /><br />" > $i.txt
    sed -i "s/<[^>]*>//g" $i.txt

    cat $i.txt >> $2
    rm $i.txt
#    rm $i.shtml
    let i++

    echo "i:$i"
    url="$path$i.shtml"
    echo "url:$url"
    output=`curl -I $path$i.shtml -s | grep "HTTP/.*OK"`;
done

echo 
echo "Success!"
