#!/bin/sh
#
# how to remove duplicate records from a file in Linux?
#
#let us consider a file with the following content. 
#The duplicate record is 'linux' with 2 entries:
#$cat file
#Unix
#Linux
#Solaris
#AIX
#Linux
#

#first solution
# sort file | uniq

#second solution
#sort -u file

#the above methods change the order of the file.
#third solution
#awk '!a[$0]++' file

#the fourth solution
#perl -lne '$x=$_;if(!grep(/^$x$/,@arr)){print; push @arr,$_;}' file

#the fifth solution
#!/bin/bash

TEMP="temp"`date '+%d%m%Y%H%M%S'`
touch $TEMP
while read line
do
    grep -q "$line" $TEMP || echo $line >> $TEMP
done < $1

cat $TEMP
