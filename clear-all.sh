#!/bin/sh

mysql -e " delete from dbinstance"
mysql -e " delete from vm "
mysql -e " delete from discardresource "
mysql -e " delete from event"
mysql -e " delete from expireddbinstance"
mysql -e " delete from product"
mysql -e " delete from securitygroup "
mysql -e " delete from securitygroupRule"
mysql -e " delete from securitygroup_dbinstance"
mysql -e " delete from snapshot"
mysql -e " delete from taskentites"
mysql -e " delete from taskflow"


for i in $(nova list | grep -i rds@ | awk '{print $2}') ; do
   nova delete $i ;
   echo "nova delete $i finished."
done

sleep 10

for i in $( nova secgroup-list | grep -i 'Default security group'  | awk '{print $2}'  ) ; do
    nova secgroup-delete $i;
done

/home/rds/rds/dev-test/tomcat-rds-selftest-Ins1/tomcat  restart
