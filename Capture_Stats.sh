#!/bin/bash

#Capture_Stats- Gather System Performance Statistics

#########################################
#Set Script Variables

REPORT_FILE=$HOME/github/script/capstats.csv
DATE=`date +%Y/%m/%d`
TIME=`date +%H:%M:%S`

#########################################
#Gather Performance Statistics

USERS=` uptime | sed 's/user.*$//g' | awk '{ print $NF}'`
LOAD=`uptime | awk '{ print $NF }'`

FREE=`vmstat 1 2 | sed  -n '/[0-9]/p' | sed -n '2p' |
awk '{ print $4}'`

IDLE=`vmstat 1 2 | sed  -n '/[0-9]/p' | sed -n '2p' |
awk '{ print $15}'`


#########################################
#Send Statistics to Report File

echo "$DATE, $TIME, $USERS, $LOAD, $FREE, $IDLE" >> $REPORT_FILE
