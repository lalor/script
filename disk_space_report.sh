#!/bin/bash
#
#Big_Users - find big disk space users in various directories
#

#parameters for script
CHECK_DIRECTORIES="/home/lalor /home/lalor/code"  #directories to check

#
#Main script
#

DATE=$(date '+%m%d%y') #date for report file

exec > disk_space_$DATE.rpt #make report file std output

echo "Top Ten Disk Space Usage"  #report header for whole report
echo "for $CHECK_DIRECTORIES directories"

for DIR_CHECK in $CHECK_DIRECTORIES  #loop to du directories
do
	echo ""
	echo "The $DIR_CHECK Directory:" #Title header for each directory
#create a listing of top ten disk space users
	du -S $DIR_CHECK  2> /dev/null | sort -nr | awk 'NR <= 10 { printf  NR ":" "\t" $1 "\t" $2 "\n" }' 

done
