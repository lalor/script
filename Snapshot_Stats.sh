#!/bin/bash
#
#Snapshot_Stats - produces a report for system stats
#

set +x
#Set Script Varibles
DATE=`date +%m%d%Y`
DISKS_TO_MONITOR="/dev/sda1 /dev"
MAIL=`which mutt`
MAIL_TO="joy_lmx@163.com"
REPORT=$HOME/Snapshot_Stats_$DATE.txt

#Create Report File

exec 3>&1 #Save file descriptor

exec 1> $REPORT  #direct output to rpt file

#################
echo
echo -e "\t\tDaily System Report"
echo

#Date Stamp The Report

DAY=`date +%d`
MONTH=`date +%m`
YEAR=`date +%Y`

echo -e "Today is  $YEAR/$MONTH/$DAY"
echo


#################
#1. Gather System uptime statistics
echo -e "System has been \c"
uptime | sed 's/,//g' | awk ' { if ( $4 == "days" || $4 == "day") { print $2, $3, $4, $5} else { print $2, $3 } }'



#################
#2. Gather Disk Usage Statistics
echo 
for DISK in $DISKS_TO_MONITOR
do
	echo -e "$DISK usage: \c"
	df -h $DISK | sed -n '/% \//p' | awk '{ print $5}'
done


#################
#3. Gather Memory Usage Statistics 
echo 
echo -e "Memory Usage:\c"
free | sed -n '2p' | awk 'x=int(($3/$2)*100){ print x}' | sed 's/$/%/'


#################
#4. Gather Number of Zombie Processes
echo 
ZOMBIE_CHECK=`ps -al | awk '{ print $2, $4}' | grep Z`
if [[ "$ZOMBIE_CHECK" = "" ]]; then
	echo "No Zombie Process on System at this Time"
else
	echo "Current System Zombie Process"
	ps -al | awk '{ print $2, $4}' | grep Z
fi
echo

#################
#5. Restore File Descriptor & Mail Report
exec 1>&3  #resotre output to STDOUT

$MAIL -a $REPORT -s "System Statistics Report for $DATE" -- $MAIL_TO </dev/null

#################
#Clean up
rm -rf $REPORT
